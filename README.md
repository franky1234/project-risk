### PROJECT RISK
  Requirements:
- Install visual studio 2017 Comunity Edition (All must have the same version)
- https://visualstudio.microsoft.com/downloads/
- The project risk is based in the framework .NET 4.5 (.NET 4.5 framwork needed)


  Project references


- Vivian in charge of:

Cards –  
Class “Cards” implementation, attributes and methods definition (including getter and setters), after that, it implemented the class “CardManager”, which has different lists of cards (game, assigned to players, changed cards )
Status: completed.

Troops –
Class “Troops” implementation, attributes and methods definition  (including getter and setters), after that, it implemented the class “TroopManager”, which has different lists of troops (game, assigned to players)
Status: In progress.

- Claudia in charge of:

Dices -
Implementation of  DiceAttack class and DiceDefence class, those classes define the logic game when player is attacking a adyacent terrotory and other one should defend.

- Patricia in charge of:

Player -
Implementation of GameManager class, PlayerManager  class and Player class, those classes define the logic game when adding new players, managing players and how these players will move on during game.

- Juan Jose in charge of:

Graphs, adyacent territories
Implementation of all concrete panels, classes related to graphs, terrotories and map. These classes applied an Iterator design pattern.

- Flanklin in charge of:

Integration -
Implementation of bussines logic of game, besides to define Abstract Strategy design patterns and support the User Stories.

- Jaime in charge of:

Map and Territories -
Implementation of Country class, Map class and Iterator design pattern.

- You can see the User Stories finished on section : Issues > Closed
- https://gitlab.com/franky1234/project-risk/issues?scope=all&utf8=%E2%9C%93&state=closed