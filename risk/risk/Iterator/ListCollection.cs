﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk.Iterator
{
    public class ListCollection : Collection<Country>
    {
        //We create a specific collection (List, Queue, Stack, Graph).
        public List<Country> countries { get; set; }

        public IteratorPattern<Country> GetEnumerator()
        {
            return new IteratorPattern<Country>(countries);
        }

        public void traverse()
        {
            IEnumerator<Country> enumerator = GetEnumerator();

            while (enumerator.MoveNext())
            {
                Country country = enumerator.Current;
                Console.WriteLine($"country name: { country.name }");
            }

        }
    }
}
