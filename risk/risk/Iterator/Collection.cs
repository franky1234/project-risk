﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk.Iterator
{
    public interface Collection<T>
    {
        IteratorPattern<T> GetEnumerator();
        void traverse();
    }
}
