﻿using System.Collections;
using System.Collections.Generic;

namespace risk.Iterator
{
    public class IteratorPattern<T> : IEnumerator<T>
    {
        //We define a specific List Iterator Pattern.
        private List<T> list { get; set; }

        private int actualPosition = -1;

        public IteratorPattern(List<T> list)
        {
            this.list = list;
        }

        public void Reset()
        {
            this.actualPosition = -1;
        }

        public void Dispose()
        {
            // Este método se implementará en caso de utilizar recursos no gestionados.
            // En ese caso, aquí se liberarán esos recursos antes de destruir el objeto.
            return;
        }

        public bool MoveNext()
        {
            bool leaveElements = (actualPosition + 1 <= this.list.Count - 1);
            if (leaveElements)
                actualPosition++;

            return leaveElements;
        }

        public T Current
        {
            get
            {
                if ((this.list == null) ||
                    (this.list.Count == 0) ||
                    (actualPosition > this.list.Count - 1) ||
                    (this.actualPosition < 0))
                    return default(T);

                else
                    return (T)this.list[actualPosition];
            }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

    }
}
