﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using risk.StrategyAbstract;

namespace risk
{
    public class ConcretePanelPlayer : AbstractPanel
    {
        public Player player { get; set; }

        public ConcretePanelPlayer(int x, int y, int widht, int height, Player player) : base(x, y, widht, height)
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
            this.player = player;
        }

        public override void addComponent()
        {
            Label labelName = new Label();
            Label labelImage = new Label();
            int x, y, sizeWidthText, sizeHeightText, sizeWidthImage, sizeHeightImage, jumpY, jumpX;

            x = 0;
            y = 0;
            sizeWidthText = 60;
            sizeHeightText = 20;
            sizeWidthImage = sizeHeightImage = 50;
            jumpY = 2;
            jumpX = 60;

            labelName.Location = new Point(x, y);
            labelName.Size = new Size(sizeWidthText, sizeHeightText);
            labelName.Text = player.name;

            labelImage.Location = new Point(x, jumpY);
            labelImage.Size = new Size(sizeWidthImage, sizeHeightImage);
            labelImage.BackColor = player.color;

            Controls.Add(labelName);
            Controls.Add(labelImage);
        }
    }
}
