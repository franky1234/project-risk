﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class GameManager
    {
        public List<Player> players { get; set; }
        public int[] sizeCountriesPerPlayer { get; set; }
        public int[] sizeTroopsPerPlayer { get; set; }
        public List<Country> countries { get; set; }
        public int sizeCountry;
        private Random position = new Random();

        public GameManager(List<Player> players, List<Country> countries)
        {
            this.players = players;
            this.countries = countries;
            sizeCountriesPerPlayer = new int[players.Count];
            sizeTroopsPerPlayer = new int[players.Count];
            sizeCountry = this.countries.Count;
        }

        public void sortCountries() {
            int counter, generateRandom, remnant;
            counter = 0;
            remnant = sizeCountry % players.Count;

            increaseSizeCountryPerPlayer(remnant);

            List<int> occuped = new List<int>();
            while (counter < sizeCountry) {
                for (int i = 0; i < players.Count; i++) {
                    generateRandom = generateRandomPosition();
                    if (!occuped.Contains(generateRandom)) {
                        if (players.ElementAt(i).positionCountries.Count < sizeCountriesPerPlayer[i]) {
                            players.ElementAt(i).positionCountries.Add(generateRandom);
                            occuped.Add(generateRandom);
                            counter += 1;
                        }
                    }
                }
            }
        }

        public void assignColorCountries() {
            players.ForEach(player => {
                player.positionCountries.ForEach(position => {
                    countries.ElementAt(position).image = player.color;
                    countries.ElementAt(position).player = player;
                });
            });
        }

        public int generateRandomPosition() {
            return position.Next(0, sizeCountry);
        }

        public void increaseSizeCountryPerPlayer(int remnant)
        {
            int INCREMENT, counter, quantityCountriesPerPlayer;
            INCREMENT= 1;
            counter = 0;
            quantityCountriesPerPlayer = sizeCountry / players.Count;

            for (int i = 0; i < sizeCountriesPerPlayer.Length; i++)
            {
                sizeCountriesPerPlayer[i] = quantityCountriesPerPlayer;
            }

            for (int i = 0; i < sizeCountriesPerPlayer.Length; i++)
            {
                if (counter < remnant)
                {
                    sizeCountriesPerPlayer[i] += INCREMENT;
                    counter += INCREMENT;
                }
            }
        }
        public void assignTerritoryTroops(int total) {
            for (int i = 0; i < sizeTroopsPerPlayer.Length; i++)
            {
                sizeTroopsPerPlayer[i] = total/sizeCountriesPerPlayer[i];
            }
            int counter = 0;
            players.ForEach(player => {
                player.positionCountries.ForEach(position => {
                    Country country = countries.ElementAt(position);
                    country.quantityTroops = sizeTroopsPerPlayer[counter];
                    
                });
                counter += 1;
            });
        }
    }
}
