﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace risk.StrategyAbstract
{
    public abstract class AbstractPanel : Panel, IPanel
    {
        protected int x { get; set; }
        protected int y { get; set; }
        protected int widht { get; set; }
        protected int height { get; set; }

        public AbstractPanel(int x, int y, int widht, int height)
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
        }

        public void createComponent()
        {
            Location = new Point(x, y);
            Size = new Size(widht, height);
            TabIndex = 0;
            addComponent();
        }
        public abstract void addComponent();
    }
}
