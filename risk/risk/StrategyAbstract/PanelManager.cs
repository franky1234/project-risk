﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk.StrategyAbstract
{
    public class PanelManager
    {
        public IPanel panel { get; set; }

        public PanelManager(IPanel panel )
        {
            this.panel = panel;
        }
        public void draw() {
            panel.createComponent();
        }
    }
}
