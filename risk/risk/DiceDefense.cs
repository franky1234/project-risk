﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class DiceDefense : Dice
    {
        public DiceDefense(Random random) : base(random)
        {
            color = Color.White;
        }
    }
}
