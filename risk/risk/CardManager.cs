﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    class CardManager
    {
        /*
        int card_count = 0; // contador de tarjetas acumuladas por jugador
        Boolean tressame;  // Controla si hay 3 tarjetas iguales
        Boolean winbatle;  // Controla si se le asigna una tarjeta si el jugador gana la batalla
        Boolean moreof5cards;  // si el jugarod tiene mas de 5 tarjetas obliga a 
        Boolean tresdifferent;
        int number_game;
        List<Card> cards_game;
        List<Card> asigned_cards;
        List<Card> changed_cards;

        public Card_Manager()
        {
            this.number_game = 0;
            cards_game = new List<Card>();
            asigned_cards = new List<Card>();
            changed_cards = new List<Card>();
        }

        public void fill_cards(int infanteria, int artilleria, int caballeria, int comodin) {

            for (int i = 1; i <= infanteria; i++) {
                cards_game.Add(new Card("card Infanteria " + i, "Infanteria"));
            }
            for (int i = 1; i <= artilleria; i++)
            {
                cards_game.Add(new Card("card artilleria " + i, "Artilleria"));
            }
            for (int i = 1; i <= caballeria; i++)
            {
                cards_game.Add(new Card("card caballeria " + i, "Caballeria"));
            }
            for (int i = 1; i <= comodin; i++)
            {
                cards_game.Add(new Card("card comodin " + i, "comodin"));
            }
        }
        public String print_cards_game() {
            String list_cards = "";
            for (int i = 0; i < cards_game.Count; i++){
                list_cards = list_cards + cards_game[i].Name_id + ", ";
            }
            return list_cards;
        }

        public Boolean assign_card(Boolean win_territory, Boolean end_turn, Player player){
            Boolean result = false;
            if (win_territory == true && end_turn == true)
            {
                Random rnd = new Random();
                int card_position = rnd.Next(1, cards_game.Count); // generate number in range 
                cards_game[card_position].Player_name = player.name; // Set Player name 
                asigned_cards.Add(cards_game[card_position]);
                cards_game.RemoveAt(card_position);
                result = true;
            }
            return result;

        }

        public Boolean change_cards(Player player) { 
            Boolean result = false;
            List<Card> cards_player = card_player(player.name);
            int artilleria = 0;
            int infanteria = 0;
            int caballeria = 0;
            int comodin = 0;
            if (cards_player.Count < 3){
                return false;
            }
            if (number_game < 7)
            {
                //recorrer la lista de cartas que tiene asignado el jugador 
                //contar cartas de forma clasificada
                for (int j = 0; j < cards_player.Count; j++)
                {
                    if (cards_player[j].Type == "Infanteria") {
                        infanteria += 1;
                    }
                    if (cards_player[j].Type == "Artilleria")
                    {
                        artilleria += 1;
                    }
                    if (cards_player[j].Type == "Caballeria")
                    {
                        caballeria += 1;
                    }
                    if (cards_player[j].Type == "comodin")
                    {
                        comodin += 1;
                    }
                }

                //verificar si el numero es mayo a 3 para cada tipo, en caso positivo, mover cartas de la lista de Assigned_cards a la lista de Changed_cards
                if (infanteria >= 3)
                {
                    int counter_remove = 0;
                    for (int i = 0; i < asigned_cards.Count; i++)
                    {
                        if (asigned_cards[i - counter_remove].Player_name == player.name && asigned_cards[i - counter_remove].Type == "Infanteria" && counter_remove <= 3) {
                            changed_cards.Add(asigned_cards[i - counter_remove]);
                            asigned_cards.RemoveAt(i - counter_remove);
                            counter_remove++;
                        }
                    }
                    return true;
                   
                }
                else if (caballeria >= 3)
                {
                    int counter_remove = 0;
                    for (int i = 0; i < asigned_cards.Count; i++)
                    {
                        if (asigned_cards[i - counter_remove].Player_name == player.name && asigned_cards[i - counter_remove].Type == "Caballeria" && counter_remove <= 3)
                        {
                            changed_cards.Add(asigned_cards[i - counter_remove]);
                            asigned_cards.RemoveAt(i - counter_remove);
                            counter_remove++;
                        }
                    }
                    return true;
                }
                else if (artilleria >= 3)
                {
                    int counter_remove = 0;
                    for (int i = 0; i < asigned_cards.Count; i++)
                    {
                        if (asigned_cards[i - counter_remove].Player_name == player.name && asigned_cards[i - counter_remove].Type == "Artilleria" && counter_remove <= 3)
                        {
                            changed_cards.Add(asigned_cards[i - counter_remove]);
                            asigned_cards.RemoveAt(i - counter_remove);
                            counter_remove++;
                        }
                    }
                    return true;
                }
                else if (infanteria >= 1 && caballeria >= 1 && artilleria >= 1)
                {
                    int counter_remove_infanteria = 0;
                    int counter_remove_caballeria = 0;
                    int counter_remove_artilleria = 0;
                    for (int i = 0; i < asigned_cards.Count; i++)
                    {
                        if (asigned_cards[i].Player_name == player.name && asigned_cards[i].Type == "infanteria" && counter_remove_infanteria < 1)
                        {
                            changed_cards.Add(asigned_cards[i]);
                            asigned_cards.RemoveAt(i);
                            counter_remove_infanteria++;
                        }
                        if (asigned_cards[i].Player_name == player.name && asigned_cards[i].Type == "caballeria" && counter_remove_caballeria < 1)
                        {
                            changed_cards.Add(asigned_cards[i]);
                            asigned_cards.RemoveAt(i);
                            counter_remove_caballeria++;
                        }
                        if (asigned_cards[i].Player_name == player.name && asigned_cards[i].Type == "artilleria" && counter_remove_artilleria < 1)
                        {
                            changed_cards.Add(asigned_cards[i]);
                            asigned_cards.RemoveAt(i);
                            counter_remove_artilleria++;
                        }

                    }
                    return true;

                }


            }
            
            return result;
        }

        public List<Card> card_player(String player_name) {
            List<Card> cards = new List<Card>();
            for (int i = 0; i < asigned_cards.Count; i++)
            {
                if (asigned_cards[i].Player_name == player_name) {
                    cards.Add(asigned_cards[i]);
                }
            }
            return cards;
        }


        public String print_cards_assigned()
        {
            String list_cards = "";
            for (int i = 0; i < asigned_cards.Count; i++)
            {
                list_cards = list_cards + asigned_cards[i].Name_id + ", ";
            }
            return list_cards;
        }

        public String print_changed_cards()
        {
            String list_cards = "";
            for (int i = 0; i < changed_cards.Count; i++)
            {
                list_cards = list_cards + changed_cards[i].Name_id + ", ";
            }
            return list_cards;
        }

        public int Number_game { get => number_game; set => number_game = value; }
        internal List<Card> Cards_game { get => cards_game; set => cards_game = value; }
        internal List<Card> Asigned_cards { get => asigned_cards; set => asigned_cards = value; }
        internal List<Card> Changed_cards { get => changed_cards; set => changed_cards = value; }
    */
    }

}
