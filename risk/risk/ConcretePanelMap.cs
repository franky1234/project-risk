﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using risk.StrategyAbstract;

namespace risk
{
    public class ConcretePanelMap : AbstractPanel
    {
        private List<Country> countries { get; set; }
        public Label[] labelNames { get; set; }
        public Label[] labelImages { get; set; }
        public Label[] labelArmies { get; set; }

        public ConcretePanelMap(List<Country> countries, int x, int y, int widht, int height) : base(x, y, widht, height)
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
            this.countries = countries;
        }

        public override void addComponent()
        {
            Label[] labelNames = new Label[countries.Count];
            Label[] labelImages = new Label[countries.Count];
            Label[] labelArmies = new Label[countries.Count];

            for (int i = 0; i < countries.Count; i++)
            {
                labelNames[i] = new Label();
                labelImages[i] = new Label();
                labelArmies[i] = new Label();
            }

            int x, y, sizeWidthText, sizeHeightText, sizeWidthImage, sizeHeightImage, jumpY, jumpX;
            x = 0;
            y = 0;
            sizeWidthText = 60;
            sizeHeightText = 20;
            sizeWidthImage = sizeHeightImage = 80;
            jumpY = 0;
            jumpX = 0;

            int contador = 0;
            for (int i = 0; i < countries.Count; i++)
            {
                labelNames[i].Location = new Point(20 + jumpX, 30 + jumpY);
                labelNames[i].Size = new Size(sizeWidthText, sizeHeightText);
                labelNames[i].Text = countries.ElementAt(i).name;
                labelNames[i].BackColor = countries.ElementAt(i).image;
                labelNames[i].ForeColor = Color.White;

                labelImages[i].Location = new Point(x + jumpX, y + jumpY);
                labelImages[i].Size = new Size(sizeWidthImage, sizeHeightImage);
                labelImages[i].BackColor = countries.ElementAt(i).image;

                labelArmies[i].Location = new Point(25 + jumpX, 45 + jumpY);
                labelArmies[i].Size = new Size(sizeWidthText - 20, sizeHeightText);
                labelArmies[i].BackColor = countries.ElementAt(i).image;
                labelArmies[i].Text = $"x{ countries.ElementAt(i).quantityTroops }";

                Controls.Add(labelArmies[i]);
                Controls.Add(labelNames[i]);
                Controls.Add(labelImages[i]);

                contador++;

                if (contador == 4)
                {
                    jumpY += 80;
                    contador = 0;
                    jumpX = -80;
                }
                jumpX += 80;
            }
        }

    }
}