﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    class DiceManager
    {
        public Player playerAttack { get; set; }
        public Player playerDefense { get; set; }

        public List<DiceAttack> diceAttacks { get; set; }
        public List<DiceDefense> diceDefenses { get; set; }

        public DiceManager(List<DiceAttack> diceAttacks, List<DiceDefense> diceDefenses)
        {
            this.diceAttacks = diceAttacks;
            this.diceDefenses = diceDefenses;
        }

        public void compareHigherDices()
        {
            diceAttacks = diceAttacks.OrderBy(dice => dice.value).ToList();
            diceDefenses = diceDefenses.OrderBy(dice => dice.value).ToList();
        }
    }
}
