﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Dice
    {
        public Color color { get; set; }
        public int value { get; set; }
        private Random random;

        public Dice(Random random)
        {
            color = Color.Black;
            this.random = random;
        }

        public void roll() {
            value = random.Next(1,7);
        }
    }
}
