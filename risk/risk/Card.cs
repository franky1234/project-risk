﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using risk.Iterator;

namespace risk
{
    public class Card
    {
        public Country countryName { get; set; }
        public Troop troopName { get; set; }
        public Color color { get; set; }

        public Card(Country countryName, Troop troopName, Color color)
        {
            this.countryName = countryName;
            this.troopName = troopName;
            this.color = color;
        }
    }
}
