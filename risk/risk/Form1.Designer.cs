﻿using System.Drawing;

using System.Collections.Generic;
using risk.Iterator;
using risk.StrategyAbstract;

namespace risk
{
    partial class Board
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 500);
            this.BackColor = Color.Beige;
            this.Name = "Board";
            this.Text = "Board-Risk";
            
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public void initializePanelManager(List<Player> players, List<Troop> troops) {
            this.SuspendLayout();
            //coords player
            int xPanelPlayer, yPanelPlayer, widthPanelPlayer, heightPanelPlayer, jumpPanelPlayerY;
            xPanelPlayer = 10;
            yPanelPlayer = 20;
            widthPanelPlayer = 60;
            heightPanelPlayer = 40;

            jumpPanelPlayerY = 0;

            int xPanelTroop, yPanelTroop, widthPanelTroop, heightPanelTroop , jumpPanelTroopY;

            //coords troop
            xPanelTroop = 180;
            yPanelTroop = 15;
            widthPanelTroop = 200;
            heightPanelTroop = 90;
            jumpPanelTroopY = 0;            

            foreach (var player in players)
            {
                AbstractPanel panelPlayer = new ConcretePanelPlayer(xPanelPlayer, yPanelPlayer + jumpPanelPlayerY, widthPanelPlayer, heightPanelPlayer, player);
                PanelManager manager = new PanelManager(panelPlayer);
                manager.draw();
                this.Controls.Add(panelPlayer);
                player.troops = troops;
                jumpPanelPlayerY += 90;

                initializeConcretePanelTroopComponent(troops, xPanelTroop, yPanelTroop + jumpPanelTroopY, widthPanelTroop, heightPanelTroop, player.color);
                jumpPanelTroopY += 90;
            }
        }

        public void initializeConcretePanelTroopComponent(List<Troop> troops, int x, int y, int width, int height, Color color) {
            this.SuspendLayout();
            troops.ForEach(troop => troop.image = color);
            AbstractPanel panelTroop = new ConcretePanelTroop(troops, x, y, width, height);
            PanelManager manager = new PanelManager(panelTroop);
            manager.draw();
            this.Controls.Add(panelTroop);
        }

        public void initializeConcretePanelMap(List<Country> countries) {
            this.SuspendLayout();

            AbstractPanel panelMap = new ConcretePanelMap(countries, 430, 100, 320, 380);
            panelMap.BackColor = Color.LightCoral;
            PanelManager manager = new PanelManager(panelMap);
            manager.draw();
            this.Controls.Add(panelMap);
        }
        
        public void initializeConcretePanelDice(Player player) {
            this.SuspendLayout();
            AbstractPanel panelDice = new ConcretePanelDice(20, 340, 305, 225, player);
            PanelManager manager = new PanelManager(panelDice);
            manager.draw();
            this.Controls.Add(panelDice);
        }

        public void initializeConcretePanelCard(List<Card> cards, Player player)
        {
            this.SuspendLayout();
            AbstractPanel panelCard = new ConcretePanelCard(cards, player, 430, 10, 80, 80);
            PanelManager manager = new PanelManager(panelCard);
            manager.draw();
            this.Controls.Add(panelCard);
            ((ConcretePanelCard)panelCard).showCard(cards[2]);
            //panelCard.BackColor = Color.Red;
        }
        #endregion
    }
}

