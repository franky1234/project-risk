﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    class Artillery : Troop
    {
        public Artillery(string name, Color image, int quantity): base(name, image, quantity)
        {
            this.equivalence = 10;
        }
    }
}
