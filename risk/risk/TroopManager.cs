﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class TroopManager
    {
        //because all we are going to have the same quantity of troops
        public Player player { get; set; }
        public int totalTroop { get; set; }

        public TroopManager(Player player)
        {
            this.player = player;
            totalTroop = 0;
        }

        public void calculateTotalTroops()
        {
            player.troops.ForEach(troop =>
            {
                totalTroop += troop.quantity;
            });
        }

        public void reduceTroops(Player playerAttack, Player playerDefense) {

        }
    }
}
