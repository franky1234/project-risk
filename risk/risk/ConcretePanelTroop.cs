﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using risk.StrategyAbstract;

namespace risk
{
    public class ConcretePanelTroop : AbstractPanel
    {
        public List<Troop> troops { get; set; }

        public ConcretePanelTroop(List<Troop> troops, int x, int y, int widht, int height) : base(x, y, widht, height)
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
            this.troops = troops;
        }

        public override void addComponent()
        {
            Label[] labelNames = new Label[troops.Count];
            Label[] labelImages = new Label[troops.Count];
            Label[] labelQuantities = new Label[troops.Count];

            for (int i = 0; i < troops.Count; i++)
            {
                labelNames[i] = new Label();
                labelImages[i] = new Label();
                labelQuantities[i] = new Label();
            }

            int x, y, sizeWidthText, sizeHeightText , sizeWidthImage, sizeHeightImage, jumpY, jumpX;
            x = 0;
            y = 0;
            sizeWidthText = 50;
            sizeHeightText = 20;
            sizeWidthImage = sizeHeightImage = 50;
            jumpY = 2;
            jumpX = 60;


            for (int i = 0; i < troops.Count; i++)
            {
                labelNames[i].Location = new Point(x, y);
                labelNames[i].Size = new Size(sizeWidthText, sizeHeightText);
                labelNames[i].Text = troops.ElementAt(i).name;

                labelImages[i].Location = new Point(x, jumpY);
                labelImages[i].Size = new Size(sizeWidthImage, sizeHeightImage);
                labelImages[i].BackColor = troops.ElementAt(i).image;

                labelQuantities[i].Location = new Point(x, sizeHeightText + sizeHeightImage - (6 * jumpY) );
                labelQuantities[i].Size = new Size(sizeWidthImage, sizeHeightImage);
                labelQuantities[i].Text = $" x { troops.ElementAt(i).quantity }";

                Controls.Add(labelNames[i]);
                Controls.Add(labelImages[i]);
                Controls.Add(labelQuantities[i]);

                x += jumpX;
            }
        }
    }
}
