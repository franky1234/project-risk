﻿using System.Drawing;
using System.Collections.Generic;

namespace risk
{
    public class Country
    {
        public string name { get; set; }
        public Color image { get; set; }
        public List<Country> Adyacents { get; set; }
        public int quantityTroops { get; set; }
        public Player player { get; set; }

        public Country(string name, Color image)
        {
            this.name = name;
            this.image = image;
            Adyacents = new List<Country>();
            quantityTroops = 0;
        }

        public bool verifyCountryAdjacent(Player other)
        {
            return player.Equals(other) ? false : true;
        }
    }
}