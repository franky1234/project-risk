﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace risk
{
    public class Player
    {
        public string name { get; set; }
        public Color color { get; set; }
        public Boolean turn { get; set; }
        public List<int> positionCountries { get; set; }
        public List<Troop> troops { get; set; }
        public Dice dice { get; set; }
        public List<Card> cards { get; set; }
        Random random = new Random();

        public Player(string name, Color color)
        {
            this.name = name;
            this.color = color;
            dice = new Dice(random);
            turn = false;
            positionCountries = new List<int>();
            troops = new List<Troop>();
            cards = new List<Card>();
        }
    }
}