﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using risk.StrategyAbstract;

namespace risk
{
    public class ConcretePanelCard: AbstractPanel
    {
        public List<Card> cardList { get; set; }

        public Player player { get; set; }

        private Label[] colorCard;
        private Label[] nameTerritory;
        private Label[] nameTroop;
        private Label totalCard;

        public ConcretePanelCard(List<Card> cardList, Player player, int x, int y, int widht, int height) : base(x, y, widht, height)
        {
            this.x = x;
            this.y = y;
            this.widht = widht;
            this.height = height;
            this.cardList = cardList;
            this.player = player;
            createLabels();
        }

        public override void addComponent()
        {
            int x, y, sizeWidthText, sizeHeightText, sizeWidthImage, sizeHeightImage;
            x = 0;
            y = 0;
            sizeWidthText = 65;
            sizeHeightText = 20;
            sizeWidthImage = sizeHeightImage = 50;

            for (int i = 0; i < cardList.Count; i++)
            {
                nameTerritory[i].Location = new Point(x + 5 , y + 5);
                nameTerritory[i].Size = new Size(sizeWidthText, sizeHeightText);
                nameTerritory[i].Text = $"{ cardList.ElementAt(i).countryName.name }";
                nameTerritory[i].BackColor = cardList.ElementAt(i).color;

                nameTroop[i].Location = new Point(x + 5, y + 30);
                nameTroop[i].Size = new Size(sizeWidthText, sizeHeightText);
                nameTroop[i].BackColor = cardList.ElementAt(i).color;
                nameTroop[i].Text = $"{ cardList.ElementAt(i).troopName.name }";

                colorCard[i].Location = new Point(x, y);
                colorCard[i].Size = new Size(sizeWidthImage + 20, sizeHeightImage + 10);
                colorCard[i].BackColor = cardList.ElementAt(i).color;

                Controls.Add(nameTerritory[i]);
                Controls.Add(nameTroop[i]);
                Controls.Add(colorCard[i]);
            }

            totalCard.Location = new Point(x + 25, y + 65);
            totalCard.Size = new Size(sizeWidthText, sizeHeightText);
            totalCard.Text = $"x{ cardList.Count }";

            Controls.Add(totalCard);
        }

        public void createLabels() {
            colorCard = new Label[cardList.Count];
            nameTerritory = new Label[cardList.Count];
            nameTroop = new Label[cardList.Count];
            totalCard = new Label();
            for (int i = 0; i < cardList.Count; i++)
            {
                colorCard[i] = new Label();
                nameTerritory[i] = new Label();
                nameTroop[i] = new Label();
            }
        }
        public void showCard(Card showCard) {

            for (int i = 0; i < cardList.Count; i++)
            {
                if (showCard.Equals(cardList.ElementAt(i)))
                {
                    colorCard[i].Visible = true;
                    nameTerritory[i].Visible = true;
                    nameTroop[i].Visible = true;
                }
                else {
                    colorCard[i].Visible = false;
                    nameTerritory[i].Visible = false;
                    nameTroop[i].Visible = false;
                }
            }
        }
    }
}
