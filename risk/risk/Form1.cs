﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using risk.Iterator;
namespace risk
{
    public partial class Board : Form
    {

        public Board()
        {
            //,new Player("Player 3", Color.Gray),new Player("Player 4", Color.Green),new Player("Player 5", Color.Purple)
            List<Player> players = new List<Player>() { new Player("Player 1", Color.Blue), new Player("Player 2", Color.Brown) };
            List<Country> countries = new List<Country>() { new Country("Bolivia", Color.Green), new Country("Argentina", Color.Blue), new Country("Brasil", Color.Gold), new Country("Chile", Color.Black), new Country("Venezuela", Color.Purple), new Country("Uruguay", Color.CornflowerBlue), new Country("Colombia", Color.GreenYellow), new Country("Peru", Color.MediumVioletRed) };
            List<Troop> troops = new List<Troop>() { new Infantery("Infantery", Color.White, 100), new Caballery("Caballery", Color.White, 50), new Artillery("Artillery", Color.White, 25) };
            List<Card> cards = new List<Card>() {
                new Card(new Country("Bolivia", Color.White),new Infantery("Infantery", Color.White, 1), Color.Orange),
                new Card(new Country("Argentina", Color.White),new Infantery("Caballery", Color.White, 1), Color.Orange),
                new Card(new Country("Brasil", Color.White),new Infantery("Artillery", Color.White, 1), Color.Orange),
                new Card(new Country("Chile", Color.White),new Infantery("Infantery", Color.White, 1), Color.Orange)
            };

            InitializeComponent();
            fillAdyacents(countries);

            PlayerManager pm = new PlayerManager(players);
            int currentPlayer = pm.startGameSecuence();
            pm.countries = countries;

            GameManager gm = new GameManager(players, countries);
            gm.sortCountries();
            gm.assignColorCountries();

            initializePanelManager(players, troops);

            TroopManager tm = new TroopManager(players[0]);
            tm.calculateTotalTroops();
            int totalTroop = tm.totalTroop;

            gm.assignTerritoryTroops(totalTroop);

            initializeConcretePanelDice(players[currentPlayer]);
            initializeConcretePanelMap(countries);
            initializeConcretePanelCard(cards, players[currentPlayer]);

            //Instantiating Iterator Pattern list
            /*ListCollection collection = new ListCollection();
            collection.countries = countries;
            collection.traverse();*/

        }

        private void fillAdyacents(List<Country> countries)
        {
            countries.ElementAt(0).Adyacents.Add(new Country("Argentina", Color.Blue));
            countries.ElementAt(0).Adyacents.Add(new Country("Venezuela", Color.Purple));
            countries.ElementAt(0).Adyacents.Add(new Country("Uruguay", Color.CornflowerBlue));

            countries.ElementAt(1).Adyacents.Add(new Country("Bolivia", Color.Green));
            countries.ElementAt(1).Adyacents.Add(new Country("Venezuela", Color.Purple));
            countries.ElementAt(1).Adyacents.Add(new Country("Uruguay", Color.CornflowerBlue));
            countries.ElementAt(1).Adyacents.Add(new Country("Colombia", Color.GreenYellow));
            countries.ElementAt(1).Adyacents.Add(new Country("Brasil", Color.Gold));

            countries.ElementAt(2).Adyacents.Add(new Country("Argentina", Color.Blue));
            countries.ElementAt(2).Adyacents.Add(new Country("Uruguay", Color.CornflowerBlue));
            countries.ElementAt(2).Adyacents.Add(new Country("Colombia", Color.GreenYellow));
            countries.ElementAt(2).Adyacents.Add(new Country("Peru", Color.MediumVioletRed));
            countries.ElementAt(2).Adyacents.Add(new Country("Chile", Color.Black));

            countries.ElementAt(3).Adyacents.Add(new Country("Brasil", Color.Gold));
            countries.ElementAt(3).Adyacents.Add(new Country("Colombia", Color.GreenYellow));
            countries.ElementAt(2).Adyacents.Add(new Country("Peru", Color.MediumVioletRed));

            countries.ElementAt(4).Adyacents.Add(new Country("Bolivia", Color.Green));
            countries.ElementAt(4).Adyacents.Add(new Country("Argentina", Color.Blue));
            countries.ElementAt(4).Adyacents.Add(new Country("Uruguay", Color.CornflowerBlue));

            countries.ElementAt(5).Adyacents.Add(new Country("Venezuela", Color.Purple));
            countries.ElementAt(5).Adyacents.Add(new Country("Bolivia", Color.Green));
            countries.ElementAt(5).Adyacents.Add(new Country("Argentina", Color.Blue));
            countries.ElementAt(5).Adyacents.Add(new Country("Brasil", Color.Gold));
            countries.ElementAt(5).Adyacents.Add(new Country("Colombia", Color.GreenYellow));

            countries.ElementAt(6).Adyacents.Add(new Country("Uruguay", Color.CornflowerBlue));
            countries.ElementAt(6).Adyacents.Add(new Country("Argentina", Color.Blue));
            countries.ElementAt(6).Adyacents.Add(new Country("Brasil", Color.Gold));
            countries.ElementAt(6).Adyacents.Add(new Country("Chile", Color.Black));
            countries.ElementAt(6).Adyacents.Add(new Country("Peru", Color.MediumVioletRed));

            countries.ElementAt(7).Adyacents.Add(new Country("Colombia", Color.GreenYellow));
            countries.ElementAt(7).Adyacents.Add(new Country("Brasil", Color.Gold));
            countries.ElementAt(7).Adyacents.Add(new Country("Chile", Color.Black));
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
